import jinja2
import os
import shutil
import yaml
import pieces

templateLoader = jinja2.FileSystemLoader("./static")
templateEnv = jinja2.Environment(loader=templateLoader)

def manual_entry(dirpath, filename, judul, git_name, git_email, color):
    # create directory for latex
    latex_dir = os.path.join(dirpath, "latex")
    os.mkdir(latex_dir)

    # append file with .tex extension
    tex_filename = filename + ".tex"
    
    output_tex = pieces.generate_output_template("laporan.jinja", templateEnv, {
        'color': color,
        'judul': judul
    })

    output_bat = pieces.generate_output_template("bat.jinja", templateEnv, {
        'color': color,
        'filename': filename
    })

    output_gitignore = pieces.generate_output_template("gitignore.jinja", templateEnv)

    pieces.write_into_file(latex_dir, tex_filename, output_tex)
    pieces.write_into_file(dirpath, "comp.bat", output_bat)
    pieces.write_into_file(dirpath, ".gitignore", output_gitignore)

    image_src = "./static/Logo_Of_EEPIS.png"
    image_dest = os.path.join(latex_dir, "Logo_Of_EEPIS.png")

    shutil.copy(image_src, image_dest)

    pieces.git_handler(dirpath, git_name, git_email)

"""
yaml structure

peator:
    - dirpath: string
      filename: string
      judul: string
      git:
        name: string
        email: string
      colors:
        - name: string
          code: string
        - ...
    - ...
"""
def yaml_entry(yaml_file):
    file = open(yaml_file, 'r')
    entry = yaml.safe_load(file) 
    file.close()
    
    for p in entry.peator:
        manual_entry(p.dirpath, p.filename, p.judul, 
        p.git.name, p.git.email, p.colors)