import PySimpleGUI as sg
import logic
from pieces import rgb_string 

color = []

manual_input = [
    [sg.Frame("Manual Input", [
            [sg.Text("Directory Path"), sg.FolderBrowse(key="dirpath")],
            [sg.Text("File name"), sg.Input(key="filename")],
            [sg.Text("Judul Laporan"), sg.Input(key="judul")],
            [
                sg.Text("Git Name"), sg.Input(key="gname"),
                sg.Text("Git Email"), sg.Input(key="gmail")
            ],
            [sg.Text("Color Name"), sg.Input(key="cname", do_not_clear=False)],
            [
                sg.Text("Color Code"), 
                sg.Input(key="ccode", do_not_clear=False), 
                sg.ColorChooserButton(button_text="choose color")
            ],
            [
                sg.Text("List Color"), 
                sg.Listbox(values=color, size=(30, 5), key="color_list"),
                sg.Button("Add", key="add_color"), 
                sg.Button("Edit", key="edit_color"), 
                sg.Button("Delete", key="delete_color")
            ],
            [sg.Button("Submit Manual Entry", key="sme")]
        ])
    ]
]

yaml_input = [
    [sg.Frame("Yaml Input", [
            [sg.Text("Yaml Definition(Optional)"), sg.FileBrowse(key="yamlf")],
            [sg.Button("Submit Yaml Definition", key="syd")]
        ])
    ]
]

layout = [
    manual_input,
    yaml_input
]

window = sg.Window("Peator", layout)

while True:
    event, values = window.read()
    if event == "add_color":
        rgb = rgb_string(values['ccode'])
        color.append({
            'name': values['cname'],
            'code': rgb
        })
        window["color_list"].update(values=color)
        window["add_color"].update("Add Color")
    elif event == "delete_color":
        color.remove(values["color_list"][0])
        window["color_list"].update(values=color)
    elif event == "edit_color":
        edit_val = values["color_list"][0]
        color.remove(values["color_list"][0])
        window["color_list"].update(values=color)
        window["cname"].update(value=edit_val["name"])
        window["ccode"].update(value=edit_val["code"])
        window["add_color"].update("Save")
    elif event == "sme":
        logic.manual_entry(values["dirpath"], 
        values["filename"], values["judul"],
        values["gname"], values["gmail"], color)
    elif event == "syd":
        logic.yaml_entry(values["yamlf"])
    elif event in (sg.WIN_CLOSED, 'Exit'):
        break


window.close()