import os
import subprocess

def write_into_file(dir, filename, output_template):
    filepath = os.path.join(dir, filename)
    file = open(filepath, "w")
    file.write(output_template)
    file.close()

def generate_output_template(template_file, template_env, render_args=None):
    template = template_env.get_template(template_file)
    if(not bool(render_args)):
        output = template.render()
        return output
    output = template.render(render_args)
    return output

def git_handler(working_dir, git_name, git_email):
    os.chdir(working_dir)
    subprocess.run(["git", "init"])
    subprocess.run(["git", "config", "user.name", git_name])
    subprocess.run(["git", "config", "user.email", git_email])
    subprocess.run(["git", "add", "."])
    subprocess.run(["git", "commit", "-m", "first init"])
    subprocess.run(["git", "branch", "-M", "master", "main"])

def rgb_string(hexval):
    h = hexval.lstrip('#')
    rgb_tuple = tuple(int(h[i:i+2], 16) for i in (0, 2, 4))
    unconcat_string = []
    for rgb in rgb_tuple:
        stri = f'{rgb},'
        unconcat_string.append(stri)
    return "".join(unconcat_string).rstrip(',')
    
    